<?php
/**
 * @copyright	Copyright (C) 2006-2013 joomleague.at. All rights reserved.
 * @license		GNU/GPL,see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License,and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * HTML View class for the Joomleague component
 *
 * @static
 * @package	JoomLeague
 * @since	0.1
 */
class JoomleagueViewProjectsExample extends JLGView
{
	protected $_name = 'projects';

	function display($tpl=null)
	{
		$this->addToolbar();

		$model = $this->getModel();

		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		// Set toolbar items for the page
		JToolBarHelper::title(JText::_('COM_JOOMLEAGUE_ADMIN_PROJECTS_TITLE'),'ProjectSettings');
		JLToolBarHelper::publishList('project.publish');
		JLToolBarHelper::unpublishList('project.unpublish');
		JToolBarHelper::divider();

		JLToolBarHelper::addNew('project.add');
		JLToolBarHelper::editList('project.edit');
		JLToolBarHelper::custom('project.import','upload','upload',Jtext::_('COM_JOOMLEAGUE_GLOBAL_CSV_IMPORT'),false);
		JLToolBarHelper::archiveList('project.export',JText::_('COM_JOOMLEAGUE_GLOBAL_XML_EXPORT'));
		JLToolBarHelper::custom('project.copy','copy.png','copy_f2.png',JText::_('COM_JOOMLEAGUE_GLOBAL_COPY'),false);
		JLToolBarHelper::deleteList(JText::_('COM_JOOMLEAGUE_ADMIN_PROJECTS_DELETE_WARNING'), 'project.remove');
		JToolBarHelper::divider();

		JToolBarHelper::help('screen.joomleague',true);
	}
}
